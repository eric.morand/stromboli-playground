import {createTwingPlugin} from "@stromboli/plugin-twing";
import {TwingLoaderChain, TwingLoaderFilesystem, TwingLoaderRelativeFilesystem} from "twing";
import {createRollupPlugin} from "@stromboli/plugin-rollup";
import createTypescriptPlugin from "@rollup/plugin-typescript";
import createNodeResolvePlugin from "@rollup/plugin-node-resolve";
import {createSassPlugin} from "@stromboli/plugin-sass";

/**
 * @type {import('@stromboli/core').Configuration}
 */
const configuration = {
    outputDirectory: 'www',
    routes: [
        {
            component: {
                path: `index.html.twig`,
                name: 'content'
            },
            plugin: createTwingPlugin(new TwingLoaderChain([
                new TwingLoaderFilesystem('.'),
                new TwingLoaderRelativeFilesystem()
            ]), {}),
            output: 'index.html'
        },
        {
            component: {
                path: `index.scss`,
                name: 'style'
            },
            plugin: createSassPlugin(),
            output: 'index.css'
        },
        {
            component: {
                path: `index.tsx`,
                name: 'behavior'
            },
            plugin: createRollupPlugin({
                plugins: [
                    createNodeResolvePlugin(),
                    createTypescriptPlugin()
                ]
            }, {}),
            output: 'index.mjs'
        }
    ]
};

export {configuration as default};