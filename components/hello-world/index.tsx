import type {FunctionComponent} from "preact";

export const HelloWorld: FunctionComponent = (props, context) => {
    return <span>Hello World!</span>;
};