import {initialize} from "../demo";
import {HelloWorld} from "../../components/hello-world";

initialize(<div className={"hello-world"}><HelloWorld/></div>);