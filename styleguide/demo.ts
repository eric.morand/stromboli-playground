import {ComponentChild, render} from "preact";

export const initialize = (
    component: ComponentChild
) => {
    document.addEventListener("DOMContentLoaded", () => {
        const container = document.getElementsByTagName('body').item(0);

        if (container) {
            return render(component, container);
        }
    });
}